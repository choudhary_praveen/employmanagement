package com.example.employmanagementcurd.repository;

import com.example.employmanagementcurd.mode.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepo extends JpaRepository<Department,Long> {
}
