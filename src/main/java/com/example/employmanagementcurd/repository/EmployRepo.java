package com.example.employmanagementcurd.repository;

import com.example.employmanagementcurd.mode.Employ;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployRepo extends JpaRepository<Employ,Long> {

    @Query("FROM tbl_employ WHERE department_id =?1")
    List<Employ> findByDepartment(@Param("depart") Long depart);
}
