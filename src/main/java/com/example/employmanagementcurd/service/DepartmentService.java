package com.example.employmanagementcurd.service;

import com.example.employmanagementcurd.mode.Department;
import com.example.employmanagementcurd.repository.DepartmentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService {
    @Autowired
    private DepartmentRepo repo;

    public void addDepartment(Department department)
    {
        repo.save(department);
    }
    public List<Department> showAllDepartment()
    {
        List<Department> list = repo.findAll();
        return list;
    }
}
