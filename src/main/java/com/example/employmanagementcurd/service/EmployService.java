package com.example.employmanagementcurd.service;

import com.example.employmanagementcurd.mode.Department;
import com.example.employmanagementcurd.mode.Employ;
import com.example.employmanagementcurd.repository.DepartmentRepo;
import com.example.employmanagementcurd.repository.EmployRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

@Service
public class EmployService {
    @Autowired
    private EmployRepo employRepo;
    @Autowired
    private DepartmentRepo departmentRepo;

    public void addEmploy(Employ e)
    {
        Optional<Department> department = departmentRepo.findById(e.getDepartment().getId());
        e.setDepartment(department.get());
        employRepo.save(e);
    }
    public Optional<Employ> getEmployById(Long id)
    {
        return employRepo.findById(id);
    }

    public Page<Employ> getAllEmploy(Pageable pageable)
    {

       // Pageable pageable = (Pageable) PageRequest.of(pageNumber,pageSize);
           return employRepo.findAll((org.springframework.data.domain.Pageable) pageable);
     }
     public List<Employ> getByDepartment(Long id)
     {
         return employRepo.findByDepartment(id);
     }

     public Optional<Employ> changeStatus(Long id)
     {
         Optional<Employ> employ = employRepo.findById(id);
         if(employ.get().isStatus())
         {
             employ.get().setStatus(false);
         }
         else if(!employ.get().isStatus()) {
             employ.get().setStatus(true);
         }
         employRepo.save(employ.get());
         return employ;
     }
     public Employ updateEmployDetails(Employ employ)
     {
         return employRepo.save(employ);
     }
     public Optional<Employ> updateDepartment(Long id,Long dept_id)
     {
         Optional<Employ> employ = employRepo.findById(id);
         if(departmentRepo.findById(dept_id).isPresent())
         {
            employ.get().setDepartment(departmentRepo.getById(dept_id));
            employRepo.save(employ.get());
         }
         return employ;

     }
}
