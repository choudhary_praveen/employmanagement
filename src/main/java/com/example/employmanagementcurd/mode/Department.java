package com.example.employmanagementcurd.mode;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "tbl_department")
@Getter
@Setter
@ToString
public class Department implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long code;
    private String name;
    //@OneToMany(mappedBy = "department",cascade = CascadeType.ALL)
   // private List<Employ> employs;

   // private List<Employ> employList;
}
