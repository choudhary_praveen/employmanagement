package com.example.employmanagementcurd.mode;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "tbl_employ")
@Table(name = "tbl_employ")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Employ {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String fname;
    private String lname;
    @ManyToOne(fetch = FetchType.LAZY,optional = false,cascade = CascadeType.ALL)
    @JoinColumn(name = "department_id",nullable = false)
    private Department department;
    private String fatherName;
    private boolean status;
    //private boolean address;
}
