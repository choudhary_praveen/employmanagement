package com.example.employmanagementcurd.controller;

import com.example.employmanagementcurd.mode.Department;
import com.example.employmanagementcurd.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DepartmentController {
    @Autowired
    private DepartmentService service;
    @PostMapping("/department")
    private ResponseEntity<Department> saveDepartment(@RequestBody Department department)
    {
        service.addDepartment(department);
        return new ResponseEntity<>(department, HttpStatus.OK);
    }
    @GetMapping("/department")
    private ResponseEntity<List<Department>> showAllDepartment()
    {
        List<Department> list = service.showAllDepartment();
        if (list.isEmpty() || list.size()==0)
        {
            return new  ResponseEntity<List<Department>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Department>>(list,HttpStatus.OK);

    }
}
