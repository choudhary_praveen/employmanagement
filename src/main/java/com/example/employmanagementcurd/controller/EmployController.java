package com.example.employmanagementcurd.controller;

import com.example.employmanagementcurd.mode.Employ;
import com.example.employmanagementcurd.service.EmployService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class EmployController {
    @Autowired
    EmployService employService;

    @PostMapping("/employ")
    public ResponseEntity<Employ> addEmploy(@RequestBody Employ employ)
    {
        employService.addEmploy(employ);
        return new ResponseEntity<Employ>(employ,HttpStatus.OK);
    }

    @GetMapping("/employ/{id}")
    public ResponseEntity<Employ> getEmployById(@PathVariable Long id)
    {
        Optional<Employ> employ = employService.getEmployById(id);
        if(employ.isPresent())
        {   return new ResponseEntity<Employ>(employ.get(),HttpStatus.OK);
        }
        return new ResponseEntity<Employ>(HttpStatus.NOT_FOUND);
    }
    @GetMapping("/employ")
    Page<Employ> findAllEmploy(Pageable page)
    {
        return employService.getAllEmploy(page);
      /*  try {


            return new ResponseEntity<Page<Employ>>(employService.getAllEmploy(pageNumber,pageSize),HttpStatus.OK);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }*/
    }
    @GetMapping("/department/{id}")
    public ResponseEntity<List<Employ>> getEmployByDepartment(@PathVariable Long id)
    {
        try {
            List<Employ> list = employService.getByDepartment(id);
            if (list.isEmpty() || list.size()==0)
            {
                return new  ResponseEntity<List<Employ>>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<List<Employ>>(list,HttpStatus.OK);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/status/{id}")
    public ResponseEntity<Employ> changeStatus(@PathVariable Long id)
    {
        Optional<Employ> employ = employService.changeStatus(id);
        if(employ.isPresent())
        {   return new ResponseEntity<Employ>(employ.get(),HttpStatus.OK);
        }
        return new ResponseEntity<Employ>(HttpStatus.NOT_FOUND);
    }
    @PutMapping("/employ")
    public ResponseEntity<Employ> updateEmploy(@RequestBody Employ employ)
    {
        try {
            return new ResponseEntity<Employ>(employService.updateEmployDetails(employ),HttpStatus.OK);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/updateDepartment")
    public ResponseEntity<Employ> updateDepartment(@RequestParam Long employId,@RequestParam Long deptId)
    {
        return new ResponseEntity<Employ>(employService.updateDepartment(employId,deptId).get(),HttpStatus.OK);
    }


}
